/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Piece.h"
#include "Board.h"
#include "Player.h"
#include "King.h"

using std::cout;
using std::endl;
using std::string;

void main()
{
	srand(time_t(NULL));

	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	int moveCounter = 1;
	int errorCode = 0;

	Player* whitePlayer = new Player("white", new King("white", new Position(7, 4)));
	Player* blackPlayer = new Player("black", new King("black", new Position(0, 4)));

	Board* board = new Board(whitePlayer, blackPlayer);

	strcpy_s(msgToGraphics, "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0"); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)
		Position* src = new Position(msgFromGraphics.substr(0, 2));
		Position* dest = new Position(msgFromGraphics.substr(2, 2));

		if (moveCounter % 2 == 1)
			errorCode = board->getBoard()[src->getX()][src->getY()]->moveIfLegal(src, dest, board, whitePlayer, blackPlayer);
		else
			errorCode = board->getBoard()[src->getX()][src->getY()]->moveIfLegal(src, dest, board, blackPlayer, whitePlayer);

		msgToGraphics[0] = (char)(errorCode + '0'); // msgToGraphics should contain the result of the operation
		msgToGraphics[1] = 0;
		if (errorCode == 0 || errorCode == 1 || errorCode == 8 || errorCode == 9 || errorCode == 10)
			moveCounter++;

		/******* JUST FOR EREZ DEBUGGING ******/
		/*int r = rand() % 10; // just for debugging......
		msgToGraphics[0] = (char)(1 + '0');
		msgToGraphics[1] = 0;
		/******* JUST FOR EREZ DEBUGGING ******/

		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}