#include "Rook.h"
#include "EmptyPiece.h"
#include "King.h"

Rook::Rook(const string color, Position* position) 
	: Piece(color == "white" ? "R" : "r", color, position){}

Rook::~Rook() {}

bool Rook::isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer) {
	if (this->isFreePath(src, dest, board))//cehcks if the path is free.
		if (src->getX() == dest->getX() || src->getY() == dest->getY())//checks if the rook moves legally.
			return true;
	return false;
}
