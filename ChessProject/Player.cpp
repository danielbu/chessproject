#include "Player.h"
#include "Board.h"
#include "Position.h"

Player::Player(string color, King* king)
	:_color(color), _king(king){}


Player::~Player(){
	delete this->_king;
}

King* Player::getKing() { 
	return this->_king;
}

string Player::getColor() const {
	return this->_color;
}

//Function will be used for checking mate or stalemate
bool Player::canMakeLegalMove(Board* board, Player* enemyPlayer) {
	int i = 0, j = 0, k = 0, l = 0;
	int errorCode = 0;
	for (i = 0; i < BOARD_LEN; i++) {
		for (j = 0; j < BOARD_LEN; j++) {
			if (board->getBoard()[i][j]->getColor() == this->_color) {
				for (k = 0; k < BOARD_LEN; k++) {
					for (l = 0; l < BOARD_LEN; l++) {
						errorCode = board->getBoard()[i][j]->isLegalTurn(new Position(i, j), new Position(k, l), board, this, enemyPlayer);
						if (errorCode == 0 || errorCode == 1 || errorCode == 8) {
							return true;
						}
					}
				}
			}
		}
	}
	return false;
}