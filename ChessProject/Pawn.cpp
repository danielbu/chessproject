#include "Pawn.h"

Pawn::Pawn(const string color, Position* position)
	: Piece(color == "white" ? "P" : "p", color, position) {}

Pawn::~Pawn() {}

bool Pawn::isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer) {
	if (this->_color == "white")
	{
		if (((!this->_hasMoved && dest->getX() - src->getX() == -2) || dest->getX() - src->getX() == -1) && dest->getY() - src->getY() == 0 && board->getBoard()[dest->getX()][dest->getY()]->getColor() != "black")
			return true;
		if (dest->getX() - src->getX() == -1 && abs(dest->getY() - src->getY()) == 1 && board->getBoard()[dest->getX()][dest->getY()]->getColor() == "black")
			return true;	
	}
	else{
		if (((!this->_hasMoved && dest->getX() - src->getX() == 2) || dest->getX() - src->getX() == 1) && dest->getY() - src->getY() == 0 && board->getBoard()[dest->getX()][dest->getY()]->getColor() != "white")
			return true;
		if (dest->getX() - src->getX() == 1 && abs(dest->getY() - src->getY()) == 1 && board->getBoard()[dest->getX()][dest->getY()]->getColor() == "white")
			return true;
	}
	return false;
}