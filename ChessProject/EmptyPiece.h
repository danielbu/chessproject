#pragma once

#include "Piece.h"

class EmptyPiece : public Piece
{
public:
	EmptyPiece(const string color, Position* position); //constructor
	~EmptyPiece(); //destructor

	virtual bool isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer);
};