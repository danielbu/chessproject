#pragma once

#include "Piece.h"

using namespace std;

#define BOARD_LEN 8

class Player;
class Piece;

class Board
{
private:
	Piece*** _board;
public:
	Board(Player* whitePlayer, Player* blackPlayer); //constructor
	~Board(); //destructor
	void setBoardToStartingPos(Player* whitePlayer, Player* blackPlayer);
	Piece*** getBoard() const;
	void setBoard(Piece*** board);
	void operator=(const Board& board);

	void printBoard() const;
};