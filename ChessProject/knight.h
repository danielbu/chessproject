#pragma once

#include "Piece.h"
using namespace std;

class Piece;

class Knight : public Piece
{
public:
	Knight(const string color, Position * position);
	~Knight();

	virtual bool isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer);

};
