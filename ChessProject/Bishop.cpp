#include "Bishop.h"
#include "EmptyPiece.h"
#include "King.h"

Bishop::Bishop(const string color, Position* position) 
	: Piece(color == "white" ? "B": "b", color, position){}

Bishop::~Bishop() {}

bool Bishop::isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer) {
	if (this->isFreePath(src, dest, board))//cehcks if the path is free.
		if (abs(src->getX() - dest->getX()) == abs(src->getY() - dest->getY()))//checks if the bishop moves legally.
			return true;
	return false;
}
