#include "EmptyPiece.h"

EmptyPiece::EmptyPiece(const string color, Position* position)
	: Piece("#", color, position) {}

EmptyPiece::~EmptyPiece() {
	delete this->_position;
}

bool EmptyPiece::isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer) {
	//empty piece can't move 
	return false;
}