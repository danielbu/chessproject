#pragma once
#include "Piece.h"

using namespace std;

class Bishop : public Piece
{
public:
	Bishop(const string color, Position* position); //constructor
	~Bishop(); //destructor

	virtual bool isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer);
};