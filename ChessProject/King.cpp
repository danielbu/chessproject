#include "King.h"
#include "EmptyPiece.h"

King::King(string color, Position* position)
	: Piece(color == "white" ? "K" : "k", color, position), _isInCheck(false) {}

King::~King(){}

bool King::isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer) {
	int errorCode = 0;
	if ((src->getX() == dest->getX() || abs(src->getX() - dest->getX()) == 1) && (src->getY() == dest->getY() || abs(src->getY() - dest->getY()) == 1))
		return true;


	//castling
	if (!this->_hasMoved && !this->_isInCheck) {
		if (dest->getY() - src->getY() == 2 && src->getX() - dest->getX() == 0 && !board->getBoard()[src->getX()][7]->hasMoved()) {
			if (this->isLegalTurn(src, new Position(src->getX(), src->getY() + 1), board, currPlayer, enemyPlayer) == legalMove) {
				this->move(src, new Position(src->getX(), src->getY() + 1), board);
				if (this->isLegalTurn(new Position(src->getX(), src->getY() + 1), dest, board, currPlayer, enemyPlayer) == legalMove) {
					this->move(new Position(src->getX(), src->getY() + 1), src, board);
					return true;
				}
				this->move(new Position(src->getX(), src->getY() + 1), src, board);
			}		
		}
		else if (dest->getY() - src->getY() == -2 && src->getX() - dest->getX() == 0 && !board->getBoard()[src->getX()][0]->hasMoved()) {
			if (this->isLegalTurn(src, new Position(src->getX(), src->getY() - 1), board, currPlayer, enemyPlayer) == legalMove) {
				this->move(src, new Position(src->getX(), src->getY() - 1), board);
				if (this->isLegalTurn(new Position(src->getX(), src->getY() - 1), dest, board, currPlayer, enemyPlayer) == legalMove && board->getBoard()[src->getX()][src->getY() - 3]->getType() == "#") {
					this->move(new Position(src->getX(), src->getY() - 1), src, board);
					return true;
				}
				this->move(new Position(src->getX(), src->getY() - 1), src, board);
			}
		}
	}
	return false;
}

bool King::isInCheck(Board* board, Player* currPlayer, Player* enemyPlayer)
{
	this->_isInCheck = false;
	for (int i = 0; i < 8 && !this->_isInCheck; i++)
	{
		for (int j = 0; j < 8 && !this->_isInCheck; j++)
		{
			if(board->getBoard()[i][j]->getColor() != this->_color && board->getBoard()[i][j]->isLegalMove(board->getBoard()[i][j]->getPosition(), this->getPosition(), board, currPlayer, enemyPlayer))
				this->_isInCheck = true;
		}
	}
	return this->_isInCheck;
}

