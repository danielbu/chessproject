#pragma once
#include "Piece.h"

using namespace std;

class Queen : public Piece
{
public:
	Queen(const string color, Position* position); //constructor
	~Queen(); //destructor

	virtual bool isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer);
};
