#pragma once

#include "Piece.h"

class Piece;

class King : public Piece
{
protected:
	bool _isInCheck;
public:
	King(string color, Position* position);
	~King();

	virtual bool isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer);
	bool isInCheck(Board * board, Player* currPlayer, Player* enemyPlayer);
};

