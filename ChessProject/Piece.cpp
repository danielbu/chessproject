#include <iostream>
#include <algorithm>
#include "Piece.h"
#include "EmptyPiece.h"
#include "King.h"
#include "Queen.h"
Piece::Piece(const string type, const string color, Position* position) 
	: _type(type), _color(color), _position(position), _hasMoved(false){}

Piece::~Piece() {
	delete this->_position;
}

Position* Piece::getPosition() const {
	return this->_position;
}

string Piece::getColor() const {
	return this->_color;
}

string Piece::getType() const {
	return this->_type;
}

bool Piece::hasMoved() const {
	return this->_hasMoved;
}

bool Piece::isFreePath(Position* src, Position* dest, Board* board) {
	int i = 0;
	if (src->getX() == dest->getX()) { //if the x value of the source and the destination doesn't change the piece moves vertically
		for (i = min(src->getY(), dest->getY()) + 1; i < max(src->getY(), dest->getY()); i++) {
			if (board->getBoard()[src->getX()][i]->getType() != "#") {
				return false;
			}
		}
	}
	else if (src->getY() == dest->getY()) { //if the y value of the source and the destination doesn't change the piece moves horizontally
		for (i = min(src->getX(), dest->getX()) + 1; i < max(src->getX(), dest->getX()); i++) {
			if (board->getBoard()[i][src->getY()]->getType() != "#") {
				return false;
			}
		}
	}
	else if (src->getX() - dest->getX() == src->getY() - dest->getY()) {//if the y value and the x value change by the same factor it means that the piece moves diagonally
		for (i = 1; i < abs(src->getX() - dest->getX()); i++) {
			if (board->getBoard()[min(src->getX(), dest->getX()) + i][min(src->getY(), dest->getY()) + i]->getType() != "#") {
				return false;
			}
		}
	}
	else if (src->getX() - dest->getX() == dest->getY() - src->getY()) {//if the y value and the x value change by the opposite factor it means that the piece moves diagonally
		for (i = 1; i < abs(src->getX() - dest->getX()); i++) {
			if (board->getBoard()[min(src->getX(), dest->getX()) + i][max(src->getY(), dest->getY()) - i]->getType() != "#") {
				return false;
			}
		}
	}
	else {
		return false;
	}
	return true;
}

int Piece::isLegalTurn(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer)
{
	int errorCode = legalMove;
	if (src == dest) {//checks if the src and dest positions are not the same square
		return sameSquare;
	}
	else if (board->getBoard()[src->getX()][src->getY()]->getColor() != currPlayer->getColor()) {
		return srcNotLegal;
	}
	else if (board->getBoard()[dest->getX()][dest->getY()]->getColor() == currPlayer->getColor()) { //check if on the destination square there is no piece of the current player.
		return destNotLegal;
	}
	else if (!src->isLegalPosition() || !dest->isLegalPosition()) {//checks if the positions are in the legal area
		return indexesOutOfRange;
	}
	else if (!this->isLegalMove(src, dest, board, currPlayer, enemyPlayer)) {//checks if the bishop moves legally and if its path is free.
		return ilegalMove;
	}
	Piece*** tmpBoard = board->getBoard();
	Piece* tmpPiece = tmpBoard[dest->getX()][dest->getY()];
	this->move(src, dest, board);

	if (currPlayer->getKing()->isInCheck(board, currPlayer, enemyPlayer))//checks if the king is in check
	{
		errorCode = selfCheck;
	}
	else if (enemyPlayer->getKing()->isInCheck(board, currPlayer, enemyPlayer)) {
		if (!enemyPlayer->canMakeLegalMove(board, currPlayer)) {
			errorCode = mate;
		}
		else {
			errorCode = check;
		}
	}
		
	//returns the board to the previous position
	move(dest, src, board);
	tmpBoard[dest->getX()][dest->getY()] = tmpPiece;
	board->setBoard(tmpBoard);

	return errorCode;
}

int Piece::moveIfLegal(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer)
{
	int error = isLegalTurn(src, dest, board, currPlayer, enemyPlayer);
	string pieceType = board->getBoard()[src->getX()][src->getY()]->getType();
	
	if (error == legalMove || error == mate || error == check) {
		//check for castling
		if (pieceType == "K" || pieceType == "k") {
			if (abs(dest->getY() - src->getY()) == 2) {
				if (dest->getY() - src->getY() == 2)
					board->getBoard()[src->getX()][7]->move(new Position(src->getX(), 7), new Position(src->getX(), 5), board);
				if (dest->getY() - src->getY() == -2)
					board->getBoard()[src->getX()][0]->move(new Position(src->getX(), 0), new Position(src->getX(), 3), board);
				this->move(src, dest, board);
				this->_hasMoved = true;
				return castle;
			}
		}
		
		this->move(src, dest, board);
		this->_hasMoved = true;
		//check for promoting
		if (pieceType == "P" || pieceType == "p") {
			if(dest->getX() == 0 || dest->getX() == 7){
				Piece*** tmpBoard = board->getBoard();
				tmpBoard[dest->getX()][dest->getY()] = new Queen(currPlayer->getColor(), dest);
				board->setBoard(tmpBoard);
				return pawnPromoted;
			}
		}
	}
	return error;
}

void Piece::move(Position* src, Position* dest, Board* board) {
	this->_position = dest;
	Piece*** tmpBoard = board->getBoard();
	tmpBoard[dest->getX()][dest->getY()] = this;
	tmpBoard[src->getX()][src->getY()] = new EmptyPiece("none", src);
	board->setBoard(tmpBoard);
}