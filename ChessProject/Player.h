#pragma once
#include<iostream>

class King;
class Board;

using namespace std;
class Player
{
private:
	string _color;
	King* _king;
public:
	Player(string color, King* _king);
	~Player();
	King* getKing();
	string getColor() const;
	bool canMakeLegalMove(Board* board, Player* enemyPlayer);
};

