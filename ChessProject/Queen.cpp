#include "Queen.h"
#include "EmptyPiece.h"
#include "King.h"

Queen::Queen(const string color, Position* position)
	: Piece(color == "white" ? "Q" : "q", color, position) {}

Queen::~Queen() {}

bool Queen::isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer) {
	return this->isFreePath(src, dest, board);//cehcks if the path is free.
}