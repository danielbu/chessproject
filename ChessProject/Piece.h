#pragma once

#include "Position.h"
#include "Board.h"
#include "Player.h"

using namespace std;

class King;
class Board;

enum errorCodes {
	legalMove, //0 - the move is legal
	check, //1 - the move is legal, the enemy king is in check
	srcNotLegal, //2 - no piece on the source square
	destNotLegal, //3 - current player's piece on destination square
	selfCheck, //4 - current player's king is in check
	indexesOutOfRange, //5 - the source or/and destination squares are out of board's range
	ilegalMove, //6 - ilegal piece movement
	sameSquare, //7 - the source and the destination are equal
	mate, //8 - the move is legal, the enemy king is in mate
	castle, //9 - the move is legal, the curr player made a castle
	pawnPromoted //10 - the move is legal, pawn turned into queen.
};

class Piece
{
protected:
	Position* _position;
	string _color;
	string _type;
	bool _hasMoved;
public:
	Piece(const string type, const string color, Position* position); //constructor
	~Piece(); //destructor

	virtual int moveIfLegal(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer); //move the piece only if it is legal
	virtual void move(Position* src, Position* dest, Board* board); //move the piece
	virtual bool isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer) = 0; //check a move if it is legal only by the piece's movement
	virtual int isLegalTurn(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer); //check a turn if it is legal
	bool isFreePath(Position* src, Position* dest, Board* board); //check if the path for the move of the piece is free

	Position* getPosition() const;
	string getColor() const;
	string getType() const;
	bool hasMoved() const;
};