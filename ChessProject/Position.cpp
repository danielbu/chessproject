#include "Position.h"

Position::Position(const string position) {
	this->_x = BOARD_LEN -  (position[1] - '0');
	this->_y = position[0] - 'a';
}

Position::Position(const int x, const int y) {
	this->_x = x;
	this->_y = y;
}

Position::~Position() {}

int Position::getX() const {
	return this->_x;
}

int Position::getY() const {
	return this->_y;
}

bool Position::isLegalPosition() {
	return !(this->_x < 0 || this->_x > BOARD_LEN-1 || this->_y < 0 || this->_y > BOARD_LEN-1);
}

bool Position::operator==(const Position& position) {
	return this->_x == position.getX() && this->_y == position.getY();
}

void Position::operator=(const Position& position) {
	this->_x = position.getX();
	this->_y = position.getY();
}