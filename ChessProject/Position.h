#pragma once

#include <iostream>

using namespace std;

#define BOARD_LEN 8

class Position
{
private:
	int _x;
	int _y;
public:
	Position(const string position); //constructor
	Position(const int x, const int y);
	~Position(); //destructor
	int getX() const;
	int getY() const;

	bool isLegalPosition(); //check if the position is inside the board's indexes

	bool operator==(const Position& position);
	void operator=(const Position& position);
};