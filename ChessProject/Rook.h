#pragma once

#include "Piece.h"

using namespace std;

class King;

class Rook : public Piece
{
public:
	Rook(const string color, Position* position); //constructor
	~Rook(); //destructor

	virtual bool isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer);
};