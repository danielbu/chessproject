#pragma once
#include "Piece.h"

using namespace std;
 
class Pawn : public Piece
{
public:
	Pawn(const string color, Position* position); //constructor
	~Pawn(); //destructor

	virtual bool isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer);
};
