#include "Board.h"
#include "King.h"
#include "Rook.h"
#include "EmptyPiece.h"
#include "Bishop.h"
#include "Queen.h"
#include "knight.h"
#include "Pawn.h"

Board::Board(Player* whitePlayer, Player* blackPlayer) {
	this->_board = new Piece * *[BOARD_LEN];
	for (int i = 0; i < BOARD_LEN; i++) {
		this->_board[i] = new Piece * [BOARD_LEN];
	}

	this->setBoardToStartingPos(whitePlayer, blackPlayer);
}

Board::~Board() {
	for (int i = 0; i < BOARD_LEN; i++) {
		delete[] this->_board[i];
	}

	delete[] this->_board;
}

void Board::setBoardToStartingPos(Player* whitePlayer, Player* blackPlayer){
	for (int i = 0; i < BOARD_LEN; i++) {
		for (int j = 0; j < BOARD_LEN; j++) {
			this->_board[i][j] = new EmptyPiece("none", new Position(i, j));
		}
	}

	//pawns
	for (int i = 0; i < BOARD_LEN; i++) {
		this->_board[1][i] = new Pawn("black", new Position(1, i));
		this->_board[6][i] = new Pawn("white", new Position(6, i));
	}

	//kings
	this->_board[0][4] = blackPlayer->getKing();
	this->_board[7][4] = whitePlayer->getKing();
	//rooks
	this->_board[0][0] = new Rook("black", new Position(0, 0));
	this->_board[0][7] = new Rook("black", new Position(0, 7));
	this->_board[7][0] = new Rook("white", new Position(7, 0));
	this->_board[7][7] = new Rook("white", new Position(7, 7));
	//bishops
	this->_board[0][2] = new Bishop("black", new Position(0, 2));
	this->_board[0][5] = new Bishop("black", new Position(0, 5));
	this->_board[7][2] = new Bishop("white", new Position(7, 5));
	this->_board[7][5] = new Bishop("white", new Position(7, 5));
	//queens
	this->_board[0][3] = new Queen("black", new Position(0, 3));
	this->_board[7][3] = new Queen("white", new Position(7, 3));
	//knights
	this->_board[0][1] = new Knight("black", new Position(0, 1));
	this->_board[0][6] = new Knight("black", new Position(0, 6));
	this->_board[7][1] = new Knight("white", new Position(7, 1));
	this->_board[7][6] = new Knight("white", new Position(7, 6));
}

Piece*** Board::getBoard() const {
	return this->_board;
}

void Board::setBoard(Piece*** board) {
	for (int i = 0; i < BOARD_LEN; i++) {
		for (int j = 0; j < BOARD_LEN; j++) {
			this->_board[i][j] = board[i][j];
		}
	}
}

void Board::operator=(const Board& board) {
	this->setBoard(board.getBoard());
}

void Board::printBoard() const {
	for (int i = 0; i < BOARD_LEN; i++) {
		for (int j = 0; j < BOARD_LEN; j++) {
			cout << this->_board[i][j]->getType() << " ";
		}
		cout << endl;
	}
}