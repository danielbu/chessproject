#include "Knight.h"
#include "EmptyPiece.h"
#include "King.h"

Knight::Knight(const string color, Position* position)
	:Piece(color == "white" ? "N" : "n", color, position) {}

Knight::~Knight() {}

bool Knight::isLegalMove(Position* src, Position* dest, Board* board, Player* currPlayer, Player* enemyPlayer)
{
	if ((abs(src->getY() - dest->getY()) == 2 && abs(src->getX() - dest->getX()) == 1) ||
		(abs(src->getX() - dest->getX()) == 2 && abs(src->getY() - dest->getY()) == 1))
		return true;
	return false;
}
